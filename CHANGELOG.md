# Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.3.1] - 2019-01-02

### Changed

- Upgraded bs-platform to 4.0.14
- Change `aria-hidden` to a string

## [0.3.0] - 2019-01-02

### Changed

- Swapped `make` and `t` aliases in `prop` and `attrs` to allow for more intuitive creation of objects
- Make all properties in both `attrs` and `prop` mutable
