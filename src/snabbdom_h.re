[@bs.deriving abstract]
type hero = {id: string};

[@bs.deriving abstract]
type vData = {
  [@bs.optional]
  mutable attrs: Snabbdom_attrs.t,
  [@bs.optional] [@bs.as "class"]
  mutable classNames: Snabbdom_class.t,
  [@bs.optional]
  mutable dataset: Snabbdom_dataset.t,
  [@bs.optional]
  mutable hero,
  [@bs.optional]
  mutable key: string,
  [@bs.optional]
  mutable ns: string,
  [@bs.optional]
  mutable props: Snabbdom_prop.t,
  [@bs.optional]
  mutable style: Snabbdom_style.t,
};

[@bs.deriving abstract]
type vnode = {
  [@bs.optional]
  mutable sel: string,
  [@bs.optional]
  mutable data: vData,
  [@bs.optional] [@bs.as "children"]
  mutable childrenNode: array(vnode),
  [@bs.optional] [@bs.as "children"]
  mutable childrenText: array(string),
  [@bs.optional]
  mutable elm: Dom.node,
  [@bs.optional]
  mutable text: string,
  [@bs.optional] [@bs.as "key"]
  mutable keyStr: string,
  [@bs.optional] [@bs.as "key"]
  mutable keyInt: int,
};

[@bs.module "snabbdom/es/h"]
external h:
  (
    string,
    vData,
    [@bs.unwrap] [ | `Content(string) | `Children(array(vnode))]
  ) =>
  vnode =
  "";