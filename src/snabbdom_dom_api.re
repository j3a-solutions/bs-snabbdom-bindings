/* open Webapi; */

[@bs.deriving]
type t = {
  createElement: string => Dom.element,
  createElementNS:
    (~namespaceURI: string, ~qualifiedName: string) => Dom.element,
  createTextNode: string => Dom.text,
  createComment: string => Dom.comment,
  insertBefore:
    (
      ~parentNode: Dom.node,
      ~newNode: Dom.node,
      ~referenceNode: option(Dom.node),
      unit
    ) =>
    unit,
  removeChild: (~node: Dom.node, ~child: Dom.node) => unit,
  appendChild: (~node: Dom.node, ~child: Dom.node) => unit,
  parentNode: Dom.node => Dom.node,
  nextSibling: Dom.node => Dom.node,
  tagName: Dom.element => string,
  setTextContent: (~node: Dom.node, ~text: option(string), unit) => unit,
  getTextContent: Dom.element => option(string),
  isElement: Dom.node => bool,
  isText: Dom.node => bool,
  isComment: Dom.node => bool,
};