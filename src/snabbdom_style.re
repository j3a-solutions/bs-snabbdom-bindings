/* Ported from reason-react */

[@bs.module "snabbdom/modules/style"]
external snabbdomModule: Snabbdom_module.t = "StyleModule";

[@bs.deriving abstract]
type t = {
  [@bs.optional]
  azimuth: string,
  [@bs.optional]
  background: string,
  [@bs.optional]
  backgroundAttachment: string,
  [@bs.optional]
  backgroundColor: string,
  [@bs.optional]
  backgroundImage: string,
  [@bs.optional]
  backgroundPosition: string,
  [@bs.optional]
  backgroundRepeat: string,
  [@bs.optional]
  border: string,
  [@bs.optional]
  borderCollapse: string,
  [@bs.optional]
  borderColor: string,
  [@bs.optional]
  borderSpacing: string,
  [@bs.optional]
  borderStyle: string,
  [@bs.optional]
  borderTop: string,
  [@bs.optional]
  borderRight: string,
  [@bs.optional]
  borderBottom: string,
  [@bs.optional]
  borderLeft: string,
  [@bs.optional]
  borderTopColor: string,
  [@bs.optional]
  borderRightColor: string,
  [@bs.optional]
  borderBottomColor: string,
  [@bs.optional]
  borderLeftColor: string,
  [@bs.optional]
  borderTopStyle: string,
  [@bs.optional]
  borderRightStyle: string,
  [@bs.optional]
  borderBottomStyle: string,
  [@bs.optional]
  borderLeftStyle: string,
  [@bs.optional]
  borderTopWidth: string,
  [@bs.optional]
  borderRightWidth: string,
  [@bs.optional]
  borderBottomWidth: string,
  [@bs.optional]
  borderLeftWidth: string,
  [@bs.optional]
  borderWidth: string,
  [@bs.optional]
  bottom: string,
  [@bs.optional]
  captionSide: string,
  [@bs.optional]
  clear: string,
  [@bs.optional]
  clip: string,
  [@bs.optional]
  color: string,
  [@bs.optional]
  content: string,
  [@bs.optional]
  counterIncrement: string,
  [@bs.optional]
  counterReset: string,
  [@bs.optional]
  cue: string,
  [@bs.optional]
  cueAfter: string,
  [@bs.optional]
  cueBefore: string,
  [@bs.optional]
  cursor: string,
  [@bs.optional]
  direction: string,
  [@bs.optional]
  display: string,
  [@bs.optional]
  elevation: string,
  [@bs.optional]
  emptyCells: string,
  [@bs.optional]
  float: string,
  [@bs.optional]
  font: string,
  [@bs.optional]
  fontFamily: string,
  [@bs.optional]
  fontSize: string,
  [@bs.optional]
  fontSizeAdjust: string,
  [@bs.optional]
  fontStretch: string,
  [@bs.optional]
  fontStyle: string,
  [@bs.optional]
  fontVariant: string,
  [@bs.optional]
  fontWeight: string,
  [@bs.optional]
  height: string,
  [@bs.optional]
  left: string,
  [@bs.optional]
  letterSpacing: string,
  [@bs.optional]
  lineHeight: string,
  [@bs.optional]
  listStyle: string,
  [@bs.optional]
  listStyleImage: string,
  [@bs.optional]
  listStylePosition: string,
  [@bs.optional]
  listStyleType: string,
  [@bs.optional]
  margin: string,
  [@bs.optional]
  marginTop: string,
  [@bs.optional]
  marginRight: string,
  [@bs.optional]
  marginBottom: string,
  [@bs.optional]
  marginLeft: string,
  [@bs.optional]
  markerOffset: string,
  [@bs.optional]
  marks: string,
  [@bs.optional]
  maxHeight: string,
  [@bs.optional]
  maxWidth: string,
  [@bs.optional]
  minHeight: string,
  [@bs.optional]
  minWidth: string,
  [@bs.optional]
  orphans: string,
  [@bs.optional]
  outline: string,
  [@bs.optional]
  outlineColor: string,
  [@bs.optional]
  outlineStyle: string,
  [@bs.optional]
  outlineWidth: string,
  [@bs.optional]
  overflow: string,
  [@bs.optional]
  overflowX: string,
  [@bs.optional]
  overflowY: string,
  [@bs.optional]
  padding: string,
  [@bs.optional]
  paddingTop: string,
  [@bs.optional]
  paddingRight: string,
  [@bs.optional]
  paddingBottom: string,
  [@bs.optional]
  paddingLeft: string,
  [@bs.optional]
  page: string,
  [@bs.optional]
  pageBreakAfter: string,
  [@bs.optional]
  pageBreakBefore: string,
  [@bs.optional]
  pageBreakInside: string,
  [@bs.optional]
  pause: string,
  [@bs.optional]
  pauseAfter: string,
  [@bs.optional]
  pauseBefore: string,
  [@bs.optional]
  pitch: string,
  [@bs.optional]
  pitchRange: string,
  [@bs.optional]
  playDuring: string,
  [@bs.optional]
  position: string,
  [@bs.optional]
  quotes: string,
  [@bs.optional]
  richness: string,
  [@bs.optional]
  right: string,
  [@bs.optional]
  size: string,
  [@bs.optional]
  speak: string,
  [@bs.optional]
  speakHeader: string,
  [@bs.optional]
  speakNumeral: string,
  [@bs.optional]
  speakPunctuation: string,
  [@bs.optional]
  speechRate: string,
  [@bs.optional]
  stress: string,
  [@bs.optional]
  tableLayout: string,
  [@bs.optional]
  textAlign: string,
  [@bs.optional]
  textDecoration: string,
  [@bs.optional]
  textIndent: string,
  [@bs.optional]
  textShadow: string,
  [@bs.optional]
  textTransform: string,
  [@bs.optional]
  top: string,
  [@bs.optional]
  unicodeBidi: string,
  [@bs.optional]
  verticalAlign: string,
  [@bs.optional]
  visibility: string,
  [@bs.optional]
  voiceFamily: string,
  [@bs.optional]
  volume: string,
  [@bs.optional]
  whiteSpace: string,
  [@bs.optional]
  widows: string,
  [@bs.optional]
  width: string,
  [@bs.optional]
  wordSpacing: string,
  [@bs.optional]
  zIndex: string,
  /* Below properties based on https://www.w3.org/Style/CSS/all-properties */
  /* Color Level 3 - REC */
  [@bs.optional]
  opacity: string,
  /* Backgrounds and Borders Level 3 - CR */
  /* backgroundRepeat - already defined by CSS2Properties */
  /* backgroundAttachment - already defined by CSS2Properties */
  [@bs.optional]
  backgroundOrigin: string,
  [@bs.optional]
  backgroundSize: string,
  [@bs.optional]
  backgroundClip: string,
  [@bs.optional]
  borderRadius: string,
  [@bs.optional]
  borderTopLeftRadius: string,
  [@bs.optional]
  borderTopRightRadius: string,
  [@bs.optional]
  borderBottomLeftRadius: string,
  [@bs.optional]
  borderBottomRightRadius: string,
  [@bs.optional]
  borderImage: string,
  [@bs.optional]
  borderImageSource: string,
  [@bs.optional]
  borderImageSlice: string,
  [@bs.optional]
  borderImageWidth: string,
  [@bs.optional]
  borderImageOutset: string,
  [@bs.optional]
  borderImageRepeat: string,
  [@bs.optional]
  boxShadow: string,
  /* Multi-column Layout - CR */
  [@bs.optional]
  columns: string,
  [@bs.optional]
  columnCount: string,
  [@bs.optional]
  columnFill: string,
  [@bs.optional]
  columnGap: string,
  [@bs.optional]
  columnRule: string,
  [@bs.optional]
  columnRuleColor: string,
  [@bs.optional]
  columnRuleStyle: string,
  [@bs.optional]
  columnRuleWidth: string,
  [@bs.optional]
  columnSpan: string,
  [@bs.optional]
  columnWidth: string,
  [@bs.optional]
  breakAfter: string,
  [@bs.optional]
  breakBefore: string,
  [@bs.optional]
  breakInside: string,
  /* Speech - CR */
  [@bs.optional]
  rest: string,
  [@bs.optional]
  restAfter: string,
  [@bs.optional]
  restBefore: string,
  [@bs.optional]
  speakAs: string,
  [@bs.optional]
  voiceBalance: string,
  [@bs.optional]
  voiceDuration: string,
  [@bs.optional]
  voicePitch: string,
  [@bs.optional]
  voiceRange: string,
  [@bs.optional]
  voiceRate: string,
  [@bs.optional]
  voiceStress: string,
  [@bs.optional]
  voiceVolume: string,
  /* Image Values and Replaced Content Level 3 - CR */
  [@bs.optional]
  objectFit: string,
  [@bs.optional]
  objectPosition: string,
  [@bs.optional]
  imageResolution: string,
  [@bs.optional]
  imageOrientation: string,
  /* Flexible Box Layout - CR */
  [@bs.optional]
  alignContent: string,
  [@bs.optional]
  alignItems: string,
  [@bs.optional]
  alignSelf: string,
  [@bs.optional]
  flex: string,
  [@bs.optional]
  flexBasis: string,
  [@bs.optional]
  flexDirection: string,
  [@bs.optional]
  flexFlow: string,
  [@bs.optional]
  flexGrow: string,
  [@bs.optional]
  flexShrink: string,
  [@bs.optional]
  flexWrap: string,
  [@bs.optional]
  justifyContent: string,
  [@bs.optional]
  order: string,
  /* Text Decoration Level 3 - CR */
  /* textDecoration - already defined by CSS2Properties */
  [@bs.optional]
  textDecorationColor: string,
  [@bs.optional]
  textDecorationLine: string,
  [@bs.optional]
  textDecorationSkip: string,
  [@bs.optional]
  textDecorationStyle: string,
  [@bs.optional]
  textEmphasis: string,
  [@bs.optional]
  textEmphasisColor: string,
  [@bs.optional]
  textEmphasisPosition: string,
  [@bs.optional]
  textEmphasisStyle: string,
  /* textShadow - already defined by CSS2Properties */
  [@bs.optional]
  textUnderlinePosition: string,
  /* Fonts Level 3 - CR */
  [@bs.optional]
  fontFeatureSettings: string,
  [@bs.optional]
  fontKerning: string,
  [@bs.optional]
  fontLanguageOverride: string,
  /* fontSizeAdjust - already defined by CSS2Properties */
  /* fontStretch - already defined by CSS2Properties */
  [@bs.optional]
  fontSynthesis: string,
  [@bs.optional]
  forntVariantAlternates: string,
  [@bs.optional]
  fontVariantCaps: string,
  [@bs.optional]
  fontVariantEastAsian: string,
  [@bs.optional]
  fontVariantLigatures: string,
  [@bs.optional]
  fontVariantNumeric: string,
  [@bs.optional]
  fontVariantPosition: string,
  /* Cascading and Inheritance Level 3 - CR */
  [@bs.optional]
  all: string,
  /* Writing Modes Level 3 - CR */
  [@bs.optional]
  glyphOrientationVertical: string,
  [@bs.optional]
  textCombineUpright: string,
  [@bs.optional]
  textOrientation: string,
  [@bs.optional]
  writingMode: string,
  /* Shapes Level 1 - CR */
  [@bs.optional]
  shapeImageThreshold: string,
  [@bs.optional]
  shapeMargin: string,
  [@bs.optional]
  shapeOutside: string,
  /* Masking Level 1 - CR */
  [@bs.optional]
  clipPath: string,
  [@bs.optional]
  clipRule: string,
  [@bs.optional]
  mask: string,
  [@bs.optional]
  maskBorder: string,
  [@bs.optional]
  maskBorderMode: string,
  [@bs.optional]
  maskBorderOutset: string,
  [@bs.optional]
  maskBorderRepeat: string,
  [@bs.optional]
  maskBorderSlice: string,
  [@bs.optional]
  maskBorderSource: string,
  [@bs.optional]
  maskBorderWidth: string,
  [@bs.optional]
  maskClip: string,
  [@bs.optional]
  maskComposite: string,
  [@bs.optional]
  maskImage: string,
  [@bs.optional]
  maskMode: string,
  [@bs.optional]
  maskOrigin: string,
  [@bs.optional]
  maskPosition: string,
  [@bs.optional]
  maskRepeat: string,
  [@bs.optional]
  maskSize: string,
  [@bs.optional]
  maskType: string,
  /* Compositing and Blending Level 1 - CR */
  [@bs.optional]
  backgroundBlendMode: string,
  [@bs.optional]
  isolation: string,
  [@bs.optional]
  mixBlendMode: string,
  /* Fragmentation Level 3 - CR */
  [@bs.optional]
  boxDecorationBreak: string,
  /* breakAfter - already defined by Multi-column Layout */
  /* breakBefore - already defined by Multi-column Layout */
  /* breakInside - already defined by Multi-column Layout */
  /* Basic User Interface Level 3 - CR */
  [@bs.optional]
  boxSizing: string,
  [@bs.optional]
  caretColor: string,
  [@bs.optional]
  navDown: string,
  [@bs.optional]
  navLeft: string,
  [@bs.optional]
  navRight: string,
  [@bs.optional]
  navUp: string,
  [@bs.optional]
  outlineOffset: string,
  [@bs.optional]
  resize: string,
  [@bs.optional]
  textOverflow: string,
  /* Grid Layout Level 1 - CR */
  [@bs.optional]
  grid: string,
  [@bs.optional]
  gridArea: string,
  [@bs.optional]
  gridAutoColumns: string,
  [@bs.optional]
  gridAutoFlow: string,
  [@bs.optional]
  gridAutoRows: string,
  [@bs.optional]
  gridColumn: string,
  [@bs.optional]
  gridColumnEnd: string,
  [@bs.optional]
  gridColumnGap: string,
  [@bs.optional]
  gridColumnStart: string,
  [@bs.optional]
  gridGap: string,
  [@bs.optional]
  gridRow: string,
  [@bs.optional]
  gridRowEnd: string,
  [@bs.optional]
  gridRowGap: string,
  [@bs.optional]
  gridRowStart: string,
  [@bs.optional]
  gridTemplate: string,
  [@bs.optional]
  gridTemplateAreas: string,
  [@bs.optional]
  gridTemplateColumns: string,
  [@bs.optional]
  gridTemplateRows: string,
  /* Will Change Level 1 - CR */
  [@bs.optional]
  willChange: string,
  /* Text Level 3 - LC */
  [@bs.optional]
  hangingPunctuation: string,
  [@bs.optional]
  hyphens: string,
  /* letterSpacing - already defined by CSS2Properties */
  [@bs.optional]
  lineBreak: string,
  [@bs.optional]
  overflowWrap: string,
  [@bs.optional]
  tabSize: string,
  /* textAlign - already defined by CSS2Properties */
  [@bs.optional]
  textAlignLast: string,
  [@bs.optional]
  textJustify: string,
  [@bs.optional]
  wordBreak: string,
  [@bs.optional]
  wordWrap: string,
  /* Animations - WD */
  [@bs.optional]
  animation: string,
  [@bs.optional]
  animationDelay: string,
  [@bs.optional]
  animationDirection: string,
  [@bs.optional]
  animationDuration: string,
  [@bs.optional]
  animationFillMode: string,
  [@bs.optional]
  animationIterationCount: string,
  [@bs.optional]
  animationName: string,
  [@bs.optional]
  animationPlayState: string,
  [@bs.optional]
  animationTimingFunction: string,
  /* Transitions - WD */
  [@bs.optional]
  transition: string,
  [@bs.optional]
  transitionDelay: string,
  [@bs.optional]
  transitionDuration: string,
  [@bs.optional]
  transitionProperty: string,
  [@bs.optional]
  transitionTimingFunction: string,
  /* Transforms Level 1 - WD */
  [@bs.optional]
  backfaceVisibility: string,
  [@bs.optional]
  perspective: string,
  [@bs.optional]
  perspectiveOrigin: string,
  [@bs.optional]
  transform: string,
  [@bs.optional]
  transformOrigin: string,
  [@bs.optional]
  transformStyle: string,
  /* Box Alignment Level 3 - WD */
  /* alignContent - already defined by Flexible Box Layout */
  /* alignItems - already defined by Flexible Box Layout */
  [@bs.optional]
  justifyItems: string,
  [@bs.optional]
  justifySelf: string,
  [@bs.optional]
  placeContent: string,
  [@bs.optional]
  placeItems: string,
  [@bs.optional]
  placeSelf: string,
  /* Basic User Interface Level 4 - FPWD */
  [@bs.optional]
  appearance: string,
  [@bs.optional]
  caret: string,
  [@bs.optional]
  caretAnimation: string,
  [@bs.optional]
  caretShape: string,
  [@bs.optional]
  userSelect: string,
  /* Overflow Level 3 - WD */
  [@bs.optional]
  maxLines: string,
  /* Basix Box Model - WD */
  [@bs.optional]
  marqueeDirection: string,
  [@bs.optional]
  marqueeLoop: string,
  [@bs.optional]
  marqueeSpeed: string,
  [@bs.optional]
  marqueeStyle: string,
  [@bs.optional]
  overflowStyle: string,
  [@bs.optional]
  rotation: string,
  [@bs.optional]
  rotationPoint: string,
  /* SVG 1.1 - REC */
  [@bs.optional]
  alignmentBaseline: string,
  [@bs.optional]
  baselineShift: string,
  /* [@bs.optional]
     clip: string,
     [@bs.optional]
     clipPath: string, */
  /* [@bs.optional]
     clipRule: string, */
  [@bs.optional]
  colorInterpolation: string,
  [@bs.optional]
  colorInterpolationFilters: string,
  [@bs.optional]
  colorProfile: string,
  [@bs.optional]
  colorRendering: string,
  /* [@bs.optional]
     cursor: string, */
  [@bs.optional]
  dominantBaseline: string,
  [@bs.optional]
  fill: string,
  [@bs.optional]
  fillOpacity: string,
  [@bs.optional]
  fillRule: string,
  [@bs.optional]
  filter: string,
  [@bs.optional]
  floodColor: string,
  [@bs.optional]
  floodOpacity: string,
  [@bs.optional]
  glyphOrientationHorizontal: string,
  /* [@bs.optional]
     glyphOrientationVertical: string, */
  [@bs.optional]
  imageRendering: string,
  [@bs.optional]
  kerning: string,
  [@bs.optional]
  lightingColor: string,
  [@bs.optional]
  markerEnd: string,
  [@bs.optional]
  markerMid: string,
  [@bs.optional]
  markerStart: string,
  [@bs.optional]
  pointerEvents: string,
  [@bs.optional]
  shapeRendering: string,
  [@bs.optional]
  stopColor: string,
  [@bs.optional]
  stopOpacity: string,
  [@bs.optional]
  stroke: string,
  [@bs.optional]
  strokeDasharray: string,
  [@bs.optional]
  strokeDashoffset: string,
  [@bs.optional]
  strokeLinecap: string,
  [@bs.optional]
  strokeLinejoin: string,
  [@bs.optional]
  strokeMiterlimit: string,
  [@bs.optional]
  strokeOpacity: string,
  [@bs.optional]
  strokeWidth: string,
  [@bs.optional]
  textAnchor: string,
  [@bs.optional]
  textRendering: string,
  /* Ruby Layout Level 1 - WD */
  [@bs.optional]
  rubyAlign: string,
  [@bs.optional]
  rubyMerge: string,
  [@bs.optional]
  rubyPosition: string,
};

type make = t;