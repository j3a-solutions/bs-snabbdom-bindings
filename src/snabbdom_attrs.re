[@bs.module "snabbdom/modules/attributes"]
external snabbdomModuleModule: Snabbdom_module.t = "AttrsModule";

[@bs.deriving abstract]
type make = {
  /* accessibility */
  /* https://www.w3.org/TR/wai-aria-1.1/ */
  /* https://accessibilityresources.org/<aria-tag> is a great resource for these */
  /* [@bs.optional] [@bs.as "aria-current"] ariaCurrent: page|step|location|date|time|true|false, */
  [@bs.optional] [@bs.as "aria-details"]
  mutable ariaDetails: string,
  [@bs.optional] [@bs.as "aria-disabled"]
  mutable ariaDisabled: bool,
  /* aria-hidden is not quite a boolean https://www.w3.org/TR/wai-aria-1.1/#aria-hidden */
  [@bs.optional] [@bs.as "aria-hidden"]
  mutable ariaHidden: string,
  /* [@bs.optional] [@bs.as "aria-invalid"] ariaInvalid: grammar|false|spelling|true, */
  [@bs.optional] [@bs.as "aria-keyshortcuts"]
  mutable ariaKeyshortcuts: string,
  [@bs.optional] [@bs.as "aria-label"]
  mutable ariaLabel: string,
  [@bs.optional] [@bs.as "aria-roledescription"]
  mutable ariaRoledescription: string,
  /* Widget Attributes */
  /* [@bs.optional] [@bs.as "aria-autocomplete"] ariaAutocomplete: inline|list|both|none, */
  /* [@bs.optional] [@bs.as "aria-checked"] ariaChecked: true|false|mixed, /* https://www.w3.org/TR/wai-aria-1.1/#valuetype_tristate */ */
  [@bs.optional] [@bs.as "aria-expanded"]
  mutable ariaExpanded: bool,
  /* [@bs.optional] [@bs.as "aria-haspopup"] ariaHaspopup: false|true|menu|listbox|tree|grid|dialog, */
  [@bs.optional] [@bs.as "aria-level"]
  mutable ariaLevel: int,
  [@bs.optional] [@bs.as "aria-modal"]
  mutable ariaModal: bool,
  [@bs.optional] [@bs.as "aria-multiline"]
  mutable ariaMultiline: bool,
  [@bs.optional] [@bs.as "aria-multiselectable"]
  mutable ariaMultiselectable: bool,
  /* [@bs.optional] [@bs.as "aria-orientation"] ariaOrientation: horizontal|vertical|undefined, */
  [@bs.optional] [@bs.as "aria-placeholder"]
  mutable ariaPlaceholder: string,
  /* [@bs.optional] [@bs.as "aria-pressed"] ariaPressed: true|false|mixed, /* https://www.w3.org/TR/wai-aria-1.1/#valuetype_tristate */ */
  [@bs.optional] [@bs.as "aria-readonly"]
  mutable ariaReadonly: bool,
  [@bs.optional] [@bs.as "aria-required"]
  mutable ariaRequired: bool,
  [@bs.optional] [@bs.as "aria-selected"]
  mutable ariaSelected: bool,
  [@bs.optional] [@bs.as "aria-sort"]
  mutable ariaSort: string,
  [@bs.optional] [@bs.as "aria-valuemax"]
  mutable ariaValuemax: float,
  [@bs.optional] [@bs.as "aria-valuemin"]
  mutable ariaValuemin: float,
  [@bs.optional] [@bs.as "aria-valuenow"]
  mutable ariaValuenow: float,
  [@bs.optional] [@bs.as "aria-valuetext"]
  mutable ariaValuetext: string,
  /* Live Region Attributes */
  [@bs.optional] [@bs.as "aria-atomic"]
  mutable ariaAtomic: bool,
  [@bs.optional] [@bs.as "aria-busy"]
  mutable ariaBusy: bool,
  /* [@bs.optional] [@bs.as "aria-live"] ariaLive: off|polite|assertive|rude, */
  [@bs.optional] [@bs.as "aria-relevant"]
  mutable ariaRelevant: string,
  /* Drag-and-Drop Attributes */
  /* [@bs.optional] [@bs.as "aria-dropeffect"] ariaDropeffect: copy|move|link|execute|popup|none, */
  [@bs.optional] [@bs.as "aria-grabbed"]
  mutable ariaGrabbed: bool,
  /* Relationship Attributes */
  [@bs.optional] [@bs.as "aria-activedescendant"]
  mutable ariaActivedescendant: string,
  [@bs.optional] [@bs.as "aria-colcount"]
  mutable ariaColcount: int,
  [@bs.optional] [@bs.as "aria-colindex"]
  mutable ariaColindex: int,
  [@bs.optional] [@bs.as "aria-colspan"]
  mutable ariaColspan: int,
  [@bs.optional] [@bs.as "aria-controls"]
  mutable ariaControls: string,
  [@bs.optional] [@bs.as "aria-describedby"]
  mutable ariaDescribedby: string,
  [@bs.optional] [@bs.as "aria-errormessage"]
  mutable ariaErrormessage: string,
  [@bs.optional] [@bs.as "aria-flowto"]
  mutable ariaFlowto: string,
  [@bs.optional] [@bs.as "aria-labelledby"]
  mutable ariaLabelledby: string,
  [@bs.optional] [@bs.as "aria-owns"]
  mutable ariaOwns: string,
  [@bs.optional] [@bs.as "aria-posinset"]
  mutable ariaPosinset: int,
  [@bs.optional] [@bs.as "aria-rowcount"]
  mutable ariaRowcount: int,
  [@bs.optional] [@bs.as "aria-rowindex"]
  mutable ariaRowindex: int,
  [@bs.optional] [@bs.as "aria-rowspan"]
  mutable ariaRowspan: int,
  [@bs.optional] [@bs.as "aria-setsize"]
  mutable ariaSetsize: int,
  /* react textarea/input */
  [@bs.optional]
  mutable defaultChecked: bool,
  [@bs.optional]
  mutable defaultValue: string,
  /* global html attributes */
  [@bs.optional]
  mutable accessKey: string,
  [@bs.optional]
  mutable className: string, /* substitute for "class" */
  [@bs.optional]
  mutable contentEditable: bool,
  [@bs.optional]
  mutable contextMenu: string,
  [@bs.optional]
  mutable dir: string, /* "ltr", "rtl" or "auto" */
  [@bs.optional]
  mutable draggable: bool,
  [@bs.optional]
  mutable hidden: bool,
  [@bs.optional]
  mutable id: string,
  [@bs.optional]
  mutable lang: string,
  [@bs.optional]
  mutable role: string, /* ARIA role */
  [@bs.optional]
  mutable spellCheck: bool,
  [@bs.optional]
  mutable tabIndex: int,
  [@bs.optional]
  mutable title: string,
  /* html5 microdata */
  [@bs.optional]
  mutable itemID: string,
  [@bs.optional]
  mutable itemProp: string,
  [@bs.optional]
  mutable itemRef: string,
  [@bs.optional]
  mutable itemScope: bool,
  [@bs.optional]
  mutable itemType: string, /* uri */
  /* tag-specific html attributes */
  [@bs.optional]
  mutable accept: string,
  [@bs.optional]
  mutable acceptCharset: string,
  [@bs.optional]
  mutable action: string, /* uri */
  [@bs.optional]
  mutable allowFullScreen: bool,
  [@bs.optional]
  mutable alt: string,
  [@bs.optional]
  mutable async: bool,
  [@bs.optional]
  mutable autoComplete: string, /* has a fixed, but large-ish, set of possible values */
  [@bs.optional]
  mutable autoFocus: bool,
  [@bs.optional]
  mutable autoPlay: bool,
  [@bs.optional]
  mutable challenge: string,
  [@bs.optional]
  mutable charSet: string,
  [@bs.optional]
  mutable checked: bool,
  [@bs.optional]
  mutable cite: string, /* uri */
  [@bs.optional]
  mutable crossorigin: bool,
  [@bs.optional]
  mutable cols: int,
  [@bs.optional]
  mutable colSpan: int,
  [@bs.optional]
  mutable content: string,
  [@bs.optional]
  mutable controls: bool,
  [@bs.optional]
  mutable coords: string, /* set of values specifying the coordinates of a region */
  [@bs.optional]
  mutable data: string, /* uri */
  [@bs.optional]
  mutable dateTime: string, /* "valid date string with optional time" */
  [@bs.optional]
  mutable default: bool,
  [@bs.optional]
  mutable defer: bool,
  [@bs.optional]
  mutable disabled: bool,
  [@bs.optional]
  mutable download: string, /* should really be either a boolean, signifying presence, or a string */
  [@bs.optional]
  mutable encType: string, /* "application/x-www-form-urlencoded", "multipart/form-data" or "text/plain" */
  [@bs.optional]
  mutable form: string,
  [@bs.optional]
  mutable formAction: string, /* uri */
  [@bs.optional]
  mutable formTarget: string, /* "_blank", "_self", etc. */
  [@bs.optional]
  mutable formMethod: string, /* "post", "get", "put" */
  [@bs.optional]
  mutable headers: string,
  [@bs.optional]
  mutable height: string, /* in html5 this can only be a number, but in html4 it can ba a percentage as well */
  [@bs.optional]
  mutable high: int,
  [@bs.optional]
  mutable href: string, /* uri */
  [@bs.optional]
  mutable hrefLang: string,
  [@bs.optional]
  mutable htmlFor: string, /* substitute for "for" */
  [@bs.optional]
  mutable httpEquiv: string, /* has a fixed set of possible values */
  [@bs.optional]
  mutable icon: string, /* uri? */
  [@bs.optional]
  mutable inputMode: string, /* "verbatim", "latin", "numeric", etc. */
  [@bs.optional]
  mutable integrity: string,
  [@bs.optional]
  mutable keyType: string,
  [@bs.optional]
  mutable kind: string, /* has a fixed set of possible values */
  [@bs.optional]
  mutable label: string,
  [@bs.optional]
  mutable list: string,
  [@bs.optional]
  mutable loop: bool,
  [@bs.optional]
  mutable low: int,
  [@bs.optional]
  mutable manifest: string, /* uri */
  [@bs.optional]
  mutable max: string, /* should be int or Js.Date.t */
  [@bs.optional]
  mutable maxLength: int,
  [@bs.optional]
  mutable media: string, /* a valid media query */
  [@bs.optional]
  mutable mediaGroup: string,
  [@bs.optional]
  mutable method: string, /* "post" or "get" */
  [@bs.optional]
  mutable min: int,
  [@bs.optional]
  mutable minLength: int,
  [@bs.optional]
  mutable multiple: bool,
  [@bs.optional]
  mutable muted: bool,
  [@bs.optional]
  mutable name: string,
  [@bs.optional]
  mutable nonce: string,
  [@bs.optional]
  mutable noValidate: bool,
  [@bs.optional] [@bs.as "open"]
  mutable open_: bool, /* use this one. Previous one is deprecated */
  [@bs.optional]
  mutable optimum: int,
  [@bs.optional]
  mutable pattern: string, /* valid Js RegExp */
  [@bs.optional]
  mutable placeholder: string,
  [@bs.optional]
  mutable poster: string, /* uri */
  [@bs.optional]
  mutable preload: string, /* "none", "metadata" or "auto" (and "" as a synonym for "auto") */
  [@bs.optional]
  mutable radioGroup: string,
  [@bs.optional]
  mutable readOnly: bool,
  [@bs.optional]
  mutable rel: string, /* a space- or comma-separated (depending on the element) list of a fixed set of "link types" */
  [@bs.optional]
  mutable required: bool,
  [@bs.optional]
  mutable reversed: bool,
  [@bs.optional]
  mutable rows: int,
  [@bs.optional]
  mutable rowSpan: int,
  [@bs.optional]
  mutable sandbox: string, /* has a fixed set of possible values */
  [@bs.optional]
  mutable scope: string, /* has a fixed set of possible values */
  [@bs.optional]
  mutable scoped: bool,
  [@bs.optional]
  mutable scrolling: string, /* html4 only, "auto", "yes" or "no" */
  /* seamless - supported by React, but removed from the html5 spec */
  [@bs.optional]
  mutable selected: bool,
  [@bs.optional]
  mutable shape: string,
  [@bs.optional]
  mutable size: int,
  [@bs.optional]
  mutable sizes: string,
  [@bs.optional]
  mutable span: int,
  [@bs.optional]
  mutable src: string, /* uri */
  [@bs.optional]
  mutable srcDoc: string,
  [@bs.optional]
  mutable srcLang: string,
  [@bs.optional]
  mutable srcSet: string,
  [@bs.optional]
  mutable start: int,
  [@bs.optional]
  mutable step: float,
  [@bs.optional]
  mutable summary: string, /* deprecated */
  [@bs.optional]
  mutable target: string,
  [@bs.optional] [@bs.as "type"]
  mutable type_: string, /* has a fixed but large-ish set of possible values */ /* use this one. Previous one is deprecated */
  [@bs.optional]
  mutable useMap: string,
  [@bs.optional]
  mutable value: string,
  [@bs.optional]
  mutable width: string, /* in html5 this can only be a number, but in html4 it can ba a percentage as well */
  [@bs.optional]
  mutable wrap: string, /* "hard" or "soft" */
  /* svg */
  [@bs.optional]
  mutable accentHeight: string,
  [@bs.optional]
  mutable accumulate: string,
  [@bs.optional]
  mutable additive: string,
  [@bs.optional]
  mutable alignmentBaseline: string,
  [@bs.optional]
  mutable allowReorder: string,
  [@bs.optional]
  mutable alphabetic: string,
  [@bs.optional]
  mutable amplitude: string,
  [@bs.optional]
  mutable arabicForm: string,
  [@bs.optional]
  mutable ascent: string,
  [@bs.optional]
  mutable attributeName: string,
  [@bs.optional]
  mutable attributeType: string,
  [@bs.optional]
  mutable autoReverse: string,
  [@bs.optional]
  mutable azimuth: string,
  [@bs.optional]
  mutable baseFrequency: string,
  [@bs.optional]
  mutable baseProfile: string,
  [@bs.optional]
  mutable baselineShift: string,
  [@bs.optional]
  mutable bbox: string,
  [@bs.optional] [@bs.as "begin"]
  mutable begin_: string, /* use this one. Previous one is deprecated */
  [@bs.optional]
  mutable bias: string,
  [@bs.optional]
  mutable by: string,
  [@bs.optional]
  mutable calcMode: string,
  [@bs.optional]
  mutable capHeight: string,
  [@bs.optional]
  mutable clip: string,
  [@bs.optional]
  mutable clipPath: string,
  [@bs.optional]
  mutable clipPathUnits: string,
  [@bs.optional]
  mutable clipRule: string,
  [@bs.optional]
  mutable colorInterpolation: string,
  [@bs.optional]
  mutable colorInterpolationFilters: string,
  [@bs.optional]
  mutable colorProfile: string,
  [@bs.optional]
  mutable colorRendering: string,
  [@bs.optional]
  mutable contentScriptType: string,
  [@bs.optional]
  mutable contentStyleType: string,
  [@bs.optional]
  mutable cursor: string,
  [@bs.optional]
  mutable cx: string,
  [@bs.optional]
  mutable cy: string,
  [@bs.optional]
  mutable d: string,
  [@bs.optional]
  mutable decelerate: string,
  [@bs.optional]
  mutable descent: string,
  [@bs.optional]
  mutable diffuseConstant: string,
  [@bs.optional]
  mutable direction: string,
  [@bs.optional]
  mutable display: string,
  [@bs.optional]
  mutable divisor: string,
  [@bs.optional]
  mutable dominantBaseline: string,
  [@bs.optional]
  mutable dur: string,
  [@bs.optional]
  mutable dx: string,
  [@bs.optional]
  mutable dy: string,
  [@bs.optional]
  mutable edgeMode: string,
  [@bs.optional]
  mutable elevation: string,
  [@bs.optional]
  mutable enableBackground: string,
  [@bs.optional] [@bs.as "end"]
  mutable end_: string, /* use this one. Previous one is deprecated */
  [@bs.optional]
  mutable exponent: string,
  [@bs.optional]
  mutable externalResourcesRequired: string,
  [@bs.optional]
  mutable fill: string,
  [@bs.optional]
  mutable fillOpacity: string,
  [@bs.optional]
  mutable fillRule: string,
  [@bs.optional]
  mutable filter: string,
  [@bs.optional]
  mutable filterRes: string,
  [@bs.optional]
  mutable filterUnits: string,
  [@bs.optional]
  mutable floodColor: string,
  [@bs.optional]
  mutable floodOpacity: string,
  [@bs.optional]
  mutable focusable: string,
  [@bs.optional]
  mutable fontFamily: string,
  [@bs.optional]
  mutable fontSize: string,
  [@bs.optional]
  mutable fontSizeAdjust: string,
  [@bs.optional]
  mutable fontStretch: string,
  [@bs.optional]
  mutable fontStyle: string,
  [@bs.optional]
  mutable fontVariant: string,
  [@bs.optional]
  mutable fontWeight: string,
  [@bs.optional]
  mutable fomat: string,
  [@bs.optional]
  mutable from: string,
  [@bs.optional]
  mutable fx: string,
  [@bs.optional]
  mutable fy: string,
  [@bs.optional]
  mutable g1: string,
  [@bs.optional]
  mutable g2: string,
  [@bs.optional]
  mutable glyphName: string,
  [@bs.optional]
  mutable glyphOrientationHorizontal: string,
  [@bs.optional]
  mutable glyphOrientationVertical: string,
  [@bs.optional]
  mutable glyphRef: string,
  [@bs.optional]
  mutable gradientTransform: string,
  [@bs.optional]
  mutable gradientUnits: string,
  [@bs.optional]
  mutable hanging: string,
  [@bs.optional]
  mutable horizAdvX: string,
  [@bs.optional]
  mutable horizOriginX: string,
  [@bs.optional]
  mutable ideographic: string,
  [@bs.optional]
  mutable imageRendering: string,
  [@bs.optional] [@bs.as "in"]
  mutable in_: string, /* use this one. Previous one is deprecated */
  [@bs.optional]
  mutable in2: string,
  [@bs.optional]
  mutable intercept: string,
  [@bs.optional]
  mutable k: string,
  [@bs.optional]
  mutable k1: string,
  [@bs.optional]
  mutable k2: string,
  [@bs.optional]
  mutable k3: string,
  [@bs.optional]
  mutable k4: string,
  [@bs.optional]
  mutable kernelMatrix: string,
  [@bs.optional]
  mutable kernelUnitLength: string,
  [@bs.optional]
  mutable kerning: string,
  [@bs.optional]
  mutable keyPoints: string,
  [@bs.optional]
  mutable keySplines: string,
  [@bs.optional]
  mutable keyTimes: string,
  [@bs.optional]
  mutable lengthAdjust: string,
  [@bs.optional]
  mutable letterSpacing: string,
  [@bs.optional]
  mutable lightingColor: string,
  [@bs.optional]
  mutable limitingConeAngle: string,
  [@bs.optional]
  mutable local: string,
  [@bs.optional]
  mutable markerEnd: string,
  [@bs.optional]
  mutable markerHeight: string,
  [@bs.optional]
  mutable markerMid: string,
  [@bs.optional]
  mutable markerStart: string,
  [@bs.optional]
  mutable markerUnits: string,
  [@bs.optional]
  mutable markerWidth: string,
  [@bs.optional]
  mutable mask: string,
  [@bs.optional]
  mutable maskContentUnits: string,
  [@bs.optional]
  mutable maskUnits: string,
  [@bs.optional]
  mutable mathematical: string,
  [@bs.optional]
  mutable mode: string,
  [@bs.optional]
  mutable numOctaves: string,
  [@bs.optional]
  mutable offset: string,
  [@bs.optional]
  mutable opacity: string,
  [@bs.optional]
  mutable operator: string,
  [@bs.optional]
  mutable order: string,
  [@bs.optional]
  mutable orient: string,
  [@bs.optional]
  mutable orientation: string,
  [@bs.optional]
  mutable origin: string,
  [@bs.optional]
  mutable overflow: string,
  [@bs.optional]
  mutable overflowX: string,
  [@bs.optional]
  mutable overflowY: string,
  [@bs.optional]
  mutable overlinePosition: string,
  [@bs.optional]
  mutable overlineThickness: string,
  [@bs.optional]
  mutable paintOrder: string,
  [@bs.optional]
  mutable panose1: string,
  [@bs.optional]
  mutable pathLength: string,
  [@bs.optional]
  mutable patternContentUnits: string,
  [@bs.optional]
  mutable patternTransform: string,
  [@bs.optional]
  mutable patternUnits: string,
  [@bs.optional]
  mutable pointerEvents: string,
  [@bs.optional]
  mutable points: string,
  [@bs.optional]
  mutable pointsAtX: string,
  [@bs.optional]
  mutable pointsAtY: string,
  [@bs.optional]
  mutable pointsAtZ: string,
  [@bs.optional]
  mutable preserveAlpha: string,
  [@bs.optional]
  mutable preserveAspectRatio: string,
  [@bs.optional]
  mutable primitiveUnits: string,
  [@bs.optional]
  mutable r: string,
  [@bs.optional]
  mutable radius: string,
  [@bs.optional]
  mutable refX: string,
  [@bs.optional]
  mutable refY: string,
  [@bs.optional]
  mutable renderingIntent: string,
  [@bs.optional]
  mutable repeatCount: string,
  [@bs.optional]
  mutable repeatDur: string,
  [@bs.optional]
  mutable requiredExtensions: string,
  [@bs.optional]
  mutable requiredFeatures: string,
  [@bs.optional]
  mutable restart: string,
  [@bs.optional]
  mutable result: string,
  [@bs.optional]
  mutable rotate: string,
  [@bs.optional]
  mutable rx: string,
  [@bs.optional]
  mutable ry: string,
  [@bs.optional]
  mutable scale: string,
  [@bs.optional]
  mutable seed: string,
  [@bs.optional]
  mutable shapeRendering: string,
  [@bs.optional]
  mutable slope: string,
  [@bs.optional]
  mutable spacing: string,
  [@bs.optional]
  mutable specularConstant: string,
  [@bs.optional]
  mutable specularExponent: string,
  [@bs.optional]
  mutable speed: string,
  [@bs.optional]
  mutable spreadMethod: string,
  [@bs.optional]
  mutable startOffset: string,
  [@bs.optional]
  mutable stdDeviation: string,
  [@bs.optional]
  mutable stemh: string,
  [@bs.optional]
  mutable stemv: string,
  [@bs.optional]
  mutable stitchTiles: string,
  [@bs.optional]
  mutable stopColor: string,
  [@bs.optional]
  mutable stopOpacity: string,
  [@bs.optional]
  mutable strikethroughPosition: string,
  [@bs.optional]
  mutable strikethroughThickness: string,
  [@bs.optional]
  mutable string,
  [@bs.optional]
  mutable stroke: string,
  [@bs.optional]
  mutable strokeDasharray: string,
  [@bs.optional]
  mutable strokeDashoffset: string,
  [@bs.optional]
  mutable strokeLinecap: string,
  [@bs.optional]
  mutable strokeLinejoin: string,
  [@bs.optional]
  mutable strokeMiterlimit: string,
  [@bs.optional]
  mutable strokeOpacity: string,
  [@bs.optional]
  mutable strokeWidth: string,
  [@bs.optional]
  mutable surfaceScale: string,
  [@bs.optional]
  mutable systemLanguage: string,
  [@bs.optional]
  mutable tableValues: string,
  [@bs.optional]
  mutable targetX: string,
  [@bs.optional]
  mutable targetY: string,
  [@bs.optional]
  mutable textAnchor: string,
  [@bs.optional]
  mutable textDecoration: string,
  [@bs.optional]
  mutable textLength: string,
  [@bs.optional]
  mutable textRendering: string,
  [@bs.optional] [@bs.as "to"]
  mutable to_: string, /* use this one. Previous one is deprecated */
  [@bs.optional]
  mutable transform: string,
  [@bs.optional]
  mutable u1: string,
  [@bs.optional]
  mutable u2: string,
  [@bs.optional]
  mutable underlinePosition: string,
  [@bs.optional]
  mutable underlineThickness: string,
  [@bs.optional]
  mutable unicode: string,
  [@bs.optional]
  mutable unicodeBidi: string,
  [@bs.optional]
  mutable unicodeRange: string,
  [@bs.optional]
  mutable unitsPerEm: string,
  [@bs.optional]
  mutable vAlphabetic: string,
  [@bs.optional]
  mutable vHanging: string,
  [@bs.optional]
  mutable vIdeographic: string,
  [@bs.optional]
  mutable vMathematical: string,
  [@bs.optional]
  mutable values: string,
  [@bs.optional]
  mutable vectorEffect: string,
  [@bs.optional]
  mutable version: string,
  [@bs.optional]
  mutable vertAdvX: string,
  [@bs.optional]
  mutable vertAdvY: string,
  [@bs.optional]
  mutable vertOriginX: string,
  [@bs.optional]
  mutable vertOriginY: string,
  [@bs.optional]
  mutable viewBox: string,
  [@bs.optional]
  mutable viewTarget: string,
  [@bs.optional]
  mutable visibility: string,
  /*width::string? =>*/
  [@bs.optional]
  mutable widths: string,
  [@bs.optional]
  mutable wordSpacing: string,
  [@bs.optional]
  mutable writingMode: string,
  [@bs.optional]
  mutable x: string,
  [@bs.optional]
  mutable x1: string,
  [@bs.optional]
  mutable x2: string,
  [@bs.optional]
  mutable xChannelSelector: string,
  [@bs.optional]
  mutable xHeight: string,
  [@bs.optional]
  mutable xlinkActuate: string,
  [@bs.optional]
  mutable xlinkArcrole: string,
  [@bs.optional]
  mutable xlinkHref: string,
  [@bs.optional]
  mutable xlinkRole: string,
  [@bs.optional]
  mutable xlinkShow: string,
  [@bs.optional]
  mutable xlinkTitle: string,
  [@bs.optional]
  mutable xlinkType: string,
  [@bs.optional]
  mutable xmlns: string,
  [@bs.optional]
  mutable xmlnsXlink: string,
  [@bs.optional]
  mutable xmlBase: string,
  [@bs.optional]
  mutable xmlLang: string,
  [@bs.optional]
  mutable xmlSpace: string,
  [@bs.optional]
  mutable y: string,
  [@bs.optional]
  mutable y1: string,
  [@bs.optional]
  mutable y2: string,
  [@bs.optional]
  mutable yChannelSelector: string,
  [@bs.optional]
  mutable z: string,
  [@bs.optional]
  mutable zoomAndPan: string,
  /* RDFa */
  [@bs.optional]
  mutable about: string,
  [@bs.optional]
  mutable datatype: string,
  [@bs.optional]
  mutable inlist: string,
  [@bs.optional]
  mutable prefix: string,
  [@bs.optional]
  mutable property: string,
  [@bs.optional]
  mutable resource: string,
  [@bs.optional]
  mutable typeof: string,
  [@bs.optional]
  mutable vocab: string,
  /* react-specific */
  [@bs.optional]
  mutable dangerouslySetInnerHTML: {. "__html": string},
  [@bs.optional]
  mutable suppressContentEditableWarning: bool,
};
type t = make;